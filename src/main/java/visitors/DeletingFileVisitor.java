import java.nio.file.Path;
import java.nio.file.FileVisitResult;
import java.io.IOException;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.FileVisitor;
import java.text.DateFormatSymbols;
import java.nio.file.attribute.FileTime;
import java.lang.Integer;
import java.nio.file.StandardCopyOption;
import java.nio.file.Files;

public class DeletingFileVisitor implements FileVisitor<Path> {
   

    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException{
        return FileVisitResult.CONTINUE;   
    }

    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException{
        Files.delete(file);
        return FileVisitResult.CONTINUE;
    }

    public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException{
        Files.delete(dir);
        return FileVisitResult.CONTINUE;
    }

    public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException{
        return FileVisitResult.CONTINUE;
    }

}
