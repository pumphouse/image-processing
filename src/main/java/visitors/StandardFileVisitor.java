import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.FileVisitResult;
import java.io.IOException;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.FileVisitor;
import java.text.DateFormatSymbols;
import java.nio.file.attribute.FileTime;
import java.nio.file.StandardCopyOption;
import java.nio.file.Files;
import java.io.File;
import java.lang.Integer;

// This will be our default file visitor, and we can change if further
// depending on future options.

public class StandardFileVisitor implements FileVisitor<Path> {
   
    Path targetDirectory;
    Path sourceDirectory;

    public StandardFileVisitor(Path sourceDirectory, Path targetDirectory){
        this.targetDirectory = targetDirectory;
        this.sourceDirectory = sourceDirectory;
    }

    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException{
        
        return FileVisitResult.CONTINUE;   
        
    }

    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException{
       
        //currently if the file exists it will be overwritte
        FileTime date  = attrs.lastModifiedTime();
        String formattedPath = getFormattedPath(date);
        
        if(file.getFileName().endsWith(".zip")){
         //http://www.lingala.net/zip4j/   
        }        

        //create the year directory if it doesn't exist
        File year = getYearDirectory(date);
        if(!year.exists()) year.mkdir();

        //create the month directory if it doesn't exist
        File month = getMonthDirectory(date);
        if(!month.exists()) month.mkdir();
        
        Path target  = Paths.get(formattedPath);
        Files.copy(file, target.resolve(file.getFileName()), StandardCopyOption.COPY_ATTRIBUTES, StandardCopyOption.REPLACE_EXISTING);

        return FileVisitResult.CONTINUE;
    }

    public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException{
        return FileVisitResult.CONTINUE;
    }

    public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException{
        return FileVisitResult.CONTINUE;
    }

    public String getMonth(int month){
        if( month < 0 || month > 12) return "error";
        return new DateFormatSymbols().getMonths()[month-1];
    }

    public File getYearDirectory(FileTime date){
        
        String fileTime = date.toString();
        String [] times = fileTime.split("-");
        String year = times[0];
        
        return new File(targetDirectory.toString() + "/" + year);
    }
    
    public File getMonthDirectory(FileTime date){
        
        String fileTime = date.toString();
        String [] times = fileTime.split("-");
        String year = times[0];
        String month = times[1];
        return new File(targetDirectory.toString() + "/" + year + "/" + getMonth(new Integer(month)));
    }

    public String getFormattedPath(FileTime date){

        String fileTime = date.toString();
        String [] times = fileTime.split("-");
        String year = times[0];
        String month = times[1];
        month = getMonth(new Integer(month));  

        return targetDirectory.toString() + "/" + year + "/" + month + "/";
    }
}
