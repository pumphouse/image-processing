import java.nio.file.Paths;
import java.nio.file.Path;
import java.nio.file.FileVisitor;
import com.lifehug.images.Enums.Visitor;

public class FileVisitorFactory{

    public static FileVisitor getFactory(Visitor type, Path source, Path target){
            
        switch (type){
            case STANDARD : return new StandardFileVisitor(source, target);
            case DELETING : return new DeletingFileVisitor();
            default: return new StandardFileVisitor(source, target);

        }

    }

}
