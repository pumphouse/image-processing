import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.io.File;
import com.lifehug.images.Enums.Visitor;
import java.nio.file.FileVisitor;

public class Navigator{
    Visitor type;

    public Navigator(Visitor type){
        this.type = type;
    }

    public void walk(String src, String trgt){
        
        final Path source = Paths.get(src);
        File file = new File(trgt);
        if(!file.exists()) file.mkdir();
        final Path target = Paths.get(file.getAbsolutePath());
       
        FileVisitor<Path> visitor = FileVisitorFactory.getFactory(type, source, target);

        try{ 
            Files.walkFileTree(source, visitor);
        } catch(IOException exc){
            System.out.println("IO Error" + exc);
            
        }
    }
}
