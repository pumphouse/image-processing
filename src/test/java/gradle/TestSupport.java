package com.lifehug.support;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.FileVisitOption;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.EnumSet;
import java.nio.file.StandardCopyOption;
import java.io.File;

public class TestSupport{
	public static void main(String[] args) throws IOException { 
            
            if( args.length < 2) return;

		    final Path sourceDir = Paths.get(args[0]);
			final Path targetDir = Paths.get(args[1]);
            
			Files.walkFileTree(sourceDir, new SimpleFileVisitor<Path>() {
		    	@Override
		        public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException  {
		            Path target = targetDir.resolve(sourceDir.relativize(dir));
                    File file = new File(target.toString());

                    if(!file.exists()){
		                 Files.copy(dir, target, StandardCopyOption.COPY_ATTRIBUTES, StandardCopyOption.REPLACE_EXISTING);
                    }

		            return FileVisitResult.CONTINUE;
		        }
		        @Override
		        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
		            Files.copy(file, targetDir.resolve(sourceDir.relativize(file)), StandardCopyOption.COPY_ATTRIBUTES, StandardCopyOption.REPLACE_EXISTING);
		            return FileVisitResult.CONTINUE;
		        }
	    	});
	
	}
}

 
