import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.junit.Test;
import java.util.*;
import org.junit.*;
import java.nio.file.Paths;
import java.nio.file.Path;
import java.io.File;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.Files;
import java.nio.file.attribute.FileTime;
import java.io.IOException;

public class StandardFileVisitorTest{
    File file;
    StandardFileVisitor visitor; 
    BasicFileAttributes attrs;
    FileTime time;
    String source;
    String target;

    @Before
    public void setUp() throws IOException {
        source = "build/resources/test/navigator";
        target = "build/resources/test/processed";   
        visitor = new StandardFileVisitor(Paths.get(source),Paths.get(target));
        file = new File("build/resources/test/standard_file_visitor/fam.jpg");
        Path path = Paths.get(file.getAbsolutePath());
        attrs = Files.readAttributes(path, BasicFileAttributes.class);     
        time = attrs.lastModifiedTime();
    }

    @Test
    public void testGetMonth(){
         assertEquals("January", visitor.getMonth(1)); 
         assertEquals("February", visitor.getMonth(2)); 
         assertEquals("March", visitor.getMonth(3)); 
         assertEquals("April", visitor.getMonth(4)); 
         assertEquals("May", visitor.getMonth(5)); 
         assertEquals("June", visitor.getMonth(6)); 
         assertEquals("July", visitor.getMonth(7)); 
         assertEquals("August", visitor.getMonth(8)); 
         assertEquals("September", visitor.getMonth(9)); 
         assertEquals("October", visitor.getMonth(10)); 
         assertEquals("November", visitor.getMonth(11)); 
         assertEquals("December", visitor.getMonth(12)); 
    }

    @Test
    public void testGetFormattedPath(){
       String path = visitor.getFormattedPath(time);
       assertEquals("path: ", "build/resources/test/processed/2015/April/", path);  
    }

    @Test
    public void testgetYearDirectory(){
        File year = visitor.getYearDirectory(time);
        assertEquals("build/resources/test/processed/2015", year.getPath());
    }

    @Test
    public void testgetMonthDirectory(){
        File month = visitor.getMonthDirectory(time);
        assertEquals("build/resources/test/processed/2015/April", month.getPath());

    }
}





