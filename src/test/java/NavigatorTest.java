import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.junit.Test;
import java.util.*;
import org.junit.*;
import java.nio.file.Paths;
import java.nio.file.Path;
import java.io.File;
import java.nio.file.FileVisitor;
import com.lifehug.images.Enums.Visitor;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;

public class NavigatorTest{
    Navigator nav;
    String source;
    String target;
    
    @Before
    public void setUp(){
        nav = new Navigator(Visitor.STANDARD);
        source = "build/resources/test/navigator/";
        target = "build/resources/test/processed/";
        nav.walk(source, target);
    }

    @Test
    public void testTraversal(){
        File file = new  File(target);
        assertTrue(file.getPath(), file.exists());
    }
    
    @Test
    public void testOneDeepFile(){
        File file =  new File(target + "2015/January/fam.jpg");
        assertTrue(target + "2015/January/fam.jpg", file.exists());
    }

    @Test
    public void testFolderOneNested(){
        File file = new File(target + "2011");
        assertTrue(target + "2011" , file.exists());
    }
    
    @Test
    public void testFolderOneNestedAgain(){
        File file = new File(target + "2011/August");
        assertTrue(target + "2011/August", file.exists());
    }


    @Test
    public void testFileOneNested(){
        File file = new File(target + "2011/August/charlee.jpg");
        assertTrue(target + "2011/August/charlee.jpg", file.exists());
    }

    @Test
    public void testFileTwoNested(){
        File file = new File(target + "2013/May/asu.jpg");
        assertTrue(target + "2013/May/asu.jpg", file.exists());
    }
    
    @Test
    public void testFolderTwoNested(){
        File file = new File(target + "2013/May");
        assertTrue(target + "2013/May", file.exists());
    }

    @Test
    public void testFilesAreSame() throws IOException {
        File file1 = new File(target + "2013/May/asu.jpg");
        File file2 = new File(source + "charlee/asu/asu.jpg");
        FileInputStream f1 = new FileInputStream(file1);
        DataInputStream d1 = new DataInputStream(f1);
        FileInputStream f2 = new FileInputStream(file2);
        DataInputStream d2 = new DataInputStream(f2);
        
        while(d2.available() > 0 && d1.available() > 0){
                byte b1 = d1.readByte();
                byte b2 = d2.readByte();

            assertTrue("Files Same", b1 == b2);    
        }

        assertTrue("Files Same", d2.available() == 0);    
        assertTrue("Files Same", d1.available() == 0);    
    }

    @Test
    public void testDuplicatesAreRemoved(){
        File file = new File(target + "2013/May");
        assertTrue("Dups Erased", file.listFiles().length == 1);
    }

    @Test 
    public void testZippedFileWasCreated(){
        File file = new File(target + "2015/April/goonies.jpg");
        assertTrue(target + "2015/April/goonies.jpg", file.exists());
    }    

    @Test 
    public void testZippedFileIceCreamWasCreated(){
        File file = new File(target + "2015/April/ice-cream.jpg");
        assertTrue(target + "2015/April/ice-cream.jpg", file.exists());
    }    

    @Test 
    public void testZippedFileKodiWasCreated(){
        File file = new File(target + "2015/April/kodi.jpg");
        assertTrue(target + "2015/April/kodi.jpg", file.exists());
    }    

    @After
    public void eraseProcessedFolder(){
        nav = new Navigator(Visitor.DELETING);
        nav.walk(target, target);
    }    
    
}





